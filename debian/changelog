octave-statistics (1.7.3-2) unstable; urgency=medium

  * test-disable-fitdist.patch: new patch, disable test that fails on amd64
  * Bump S-V to 4.7.1

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 20 Feb 2025 16:28:51 +0100

octave-statistics (1.7.3-1) unstable; urgency=medium

  * New upstream version 1.7.3
  * d/copyright: reflect upstream changes
  * tests-disabled-for-debCI.patch: drop patch.
    Possibly no longer needed since upstream release 1.7.2.

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 19 Feb 2025 11:36:29 +0100

octave-statistics (1.7.2-1) unstable; urgency=medium

  * New upstream version 1.7.2
  * d/p/test-ClassificationPartitionedModel.patch: Drop patch (applied upstream)
  * d/p/tests-disabled-for-debCI.patch: Refresh for new upstream version
  * d/copyright: Update list of copyright years

 -- Rafael Laboissière <rafael@debian.org>  Wed, 05 Feb 2025 23:03:56 -0300

octave-statistics (1.7.0-5) unstable; urgency=medium

  * tests-disabled-for-debCI.patch: disable a few more tests

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 19 Jan 2025 12:07:56 +0100

octave-statistics (1.7.0-4) unstable; urgency=medium

  * tests-disabled-for-debCI.patch: disable more slow tests on debCI/amd64
    (Closes: #1091873)

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 19 Jan 2025 11:40:53 +0100

octave-statistics (1.7.0-3) unstable; urgency=medium

  * test-ClassificationPartitionedModel.patch: update following upstream fix
  * test-ClassificationPartitionedModel-2.patch: new patch.
    Should fix random autopkgtest timeouts. (Closes: #1091873)

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 12 Jan 2025 19:17:09 +0100

octave-statistics (1.7.0-2) unstable; urgency=medium

  * test-ClassificationPartitionedModel.patch: new patch.
    Disables tests that randomly enters an infinite loop. (Closes: #1081727)

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 16 Sep 2024 18:55:03 +0200

octave-statistics (1.7.0-1) unstable; urgency=medium

  * New upstream version 1.7.0
  * d/copyright: reflect upstream changes
  * Generated HTML is no longer shipped with the package.
    It is no longer included in the upstream tarball. Documentation remains
    accessible online or via the “help” command in Octave.
  * Ship README.md and old upstream changelogs
  * Fix cleaning of empty doc/ directory.
    Also remove workaround for fixing incorrect permissions in upstream tarball,
    no longer needed.

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 04 Sep 2024 11:33:13 +0200

octave-statistics (1.6.7-1) unstable; urgency=medium

  * New upstream version 1.6.7

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 30 Jun 2024 16:20:41 +0200

octave-statistics (1.6.6-2) unstable; urgency=medium

  * debian/checkvars: do not run BISTs in mad.m.
    These tests fail in an autopkgtest context with Octave 9. And actually this
    file is not made available under Octave ⩾ 9, because it is under the
    shadow9/ subdirectory. So we can simply ignore it.

 -- Sébastien Villemot <sebastien@debian.org>  Tue, 21 May 2024 15:01:32 +0200

octave-statistics (1.6.6-1) unstable; urgency=medium

  * New upstream version 1.6.6
  * d/control: Bump Standards-Version to 4.7.0 (no changes needed)
  * Drop patch (applied upstream):
    + d/p/glmfit-reproducible-bist.patch
    + d/p/binomial-dist-median.patch
    + d/p/fpdf-reproducile-bist.patch
  * d/s/lintian-overrides: Add override for missing source of file
    editDistance.html

 -- Rafael Laboissière <rafael@debian.org>  Sat, 18 May 2024 05:35:21 -0300

octave-statistics (1.6.5-3) unstable; urgency=medium

  * d/p/glmfit-reproducible-bist.patch: Add Applied-Upstream field
  * d/p/fpdf-reproducile-bist.patch: New patch (Closes: #1067327)
  * d/p/binomial-dist-median.patch: Update with upstream commit

 -- Rafael Laboissière <rafael@debian.org>  Fri, 22 Mar 2024 07:33:29 -0300

octave-statistics (1.6.5-2) unstable; urgency=medium

  * d/p/binomial-dist-median.patch: New patch

 -- Rafael Laboissière <rafael@debian.org>  Sun, 10 Mar 2024 03:57:17 -0300

octave-statistics (1.6.5-1) unstable; urgency=medium

  * New upstream version 1.6.5
  * d/p/glmfit-reproducible-bist.patch: New patch

 -- Rafael Laboissière <rafael@debian.org>  Sat, 09 Mar 2024 03:00:06 -0300

octave-statistics (1.6.4-3) unstable; urgency=medium

  * d/rules: Instead of the execute_after_dh_installdeb target, use the
    -indep variant.
    The commands in this rule are intended to be run on the files
    installed by the octave-statistics-common package and fail in arch:any
    builds. (Really, closes: #1065548)

 -- Rafael Laboissière <rafael@debian.org>  Fri, 08 Mar 2024 08:14:53 -0300

octave-statistics (1.6.4-2) unstable; urgency=medium

  * d/rules: Do not FTBFS if empty doc/ directory does not exist
    (Closes: #1065548)

 -- Rafael Laboissière <rafael@debian.org>  Wed, 06 Mar 2024 13:25:56 -0300

octave-statistics (1.6.4-1) unstable; urgency=medium

  * New upstream version 1.6.4
  * d/p/xtest-bist-pdist2.patch: Drop patch (not needed anymore)
  * d/rules:
    + Run the unit tests in dendrogram.m
    + Remove empty directory doc/
    + Change wrong permission of *.m files

 -- Rafael Laboissière <rafael@debian.org>  Tue, 05 Mar 2024 23:14:11 -0300

octave-statistics (1.6.3-1) unstable; urgency=medium

  * New upstream version 1.6.3
  * Drop patches:
    + d/p/test-failures.patch: Not needed anymore
    + d/p/test-failures-cameratarget.patch: Not needed anymore
    + d/p/xtest-bists-with-plots.patch: Applied upstream
    + d/p/xtest-bist-fitcknn.patch: Applied upstream
    + d/p/xtest-bist-pdist2.patch: Not needed anymore
  * d/control: Depend on libjs-bootstrap5, instead of libjs-bootstrap
  * d/local-url:
    + Fix URL of JavaScript and CSS files
    + Remove properties integrity, crossorigin, and referrerpolicy

 -- Rafael Laboissière <rafael@debian.org>  Sun, 11 Feb 2024 15:18:39 -0300

octave-statistics (1.6.1-2) unstable; urgency=medium

  * d/p/xtest-bist-pdist2.patch: Mark a further failing BIST in
    pdist2.m as xtest.  This seems to be necessary to avoid FTBFS on
    arm64, ppc64el, riscv64, and r390x.

 -- Rafael Laboissière <rafael@debian.org>  Sat, 20 Jan 2024 14:38:09 -0300

octave-statistics (1.6.1-1) unstable; urgency=medium

  * New upstream version 1.6.1
  * d/copyright: Reflect upstream changes
  * d/p/normfit-bist-tolerance.patch: Drop patch (applied upstream)
  * d/p/more-bist-tolerance.patch: Drop patch (applied upstream)
  * d/p/test-failures-cameratarget.patch: Mark further failing BISTS as xtest
  * d/p/xtest-bists-with-plots.patch: New patch
  * Split package into arch-dep and arch-indep packages.
    Lintian indicates that his should be done, via the new warning
    arch-dep-package-has-big-usr-share.
    + d/control: Add stanza for octave-statistics-common and make
      octave-statistics depend on it
    + d/octave-statistics-common.install: New file
    + d/octave-statistics.install: New file
  * Install documentation
    + d/octave-statistics-common.docs: New file
    + d/octave-statistics-common.doc-base: New file
    + d/local-url: New script for replacing JavaScript external URLs
      by local ones
    + d/control:
      - Build-depend on python3 (for running d/local-url)
      - Make octave-statistics-common depend on libjs-bootstrap and
        libjs-mathjax
    + d/rules: Run d/local-url after dh_installdeb
  * d/rules: Exclude dendogram.m from tests
  * d/watch: Drop filemangle (Lintian warning prefer-uscan-symlink)
  * d/p/xtest-bist-fitcknn.patch: New patch
  * d/p/xtest-bist-pdist2.patch: New patch

 -- Rafael Laboissière <rafael@debian.org>  Wed, 17 Jan 2024 15:30:48 -0300

octave-statistics (1.6.0-2) unstable; urgency=medium

  * test-failures-cameratarget.patch: new patch by Graham Inggs.
    Taken from Ubuntu. Fixes random autopkgtest failures. (Closes: #1042933)

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 02 Sep 2023 15:59:18 +0200

octave-statistics (1.6.0-1) unstable; urgency=medium

  [ Rafael Laboissière ]
  * New upstream version 1.6.0
  * d/p/test-normalise_distribution-tolerance.patch: Drop patch (applied
    upstream)
  * d/p/test-tolerance.patch: Drop patch (applied upstream)
  * d/p/test-hotelling_t2test-deterministic.patch; Drop patch (applied upstream)
  * d/p/test-further-tolerance.patch: Drop patch (applied upstream)
  * d/p/normfit-bist-tolerance.patch: New patch
  * d/u/metadata: Add Changelog and Documentation URLs

  [ Sébastien Villemot ]
  * more-bist-tolerance.patch: new patch, fixes various BISTs on amd64 and i386

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 17 Jun 2023 13:10:49 +0200

octave-statistics (1.5.4-5) experimental; urgency=medium

  * test-further-tolerance.patch: loosen some tolerance again

 -- Sébastien Villemot <sebastien@debian.org>  Tue, 25 Apr 2023 15:38:27 +0200

octave-statistics (1.5.4-4) experimental; urgency=medium

  * test-further-tolerance.patch: new patch

 -- Sébastien Villemot <sebastien@debian.org>  Tue, 25 Apr 2023 14:49:26 +0200

octave-statistics (1.5.4-3) experimental; urgency=medium

  * test-tolerance.patch: loosen another numerical test
  * test-hotelling_t2test-deterministic.patch: new patch

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 24 Apr 2023 18:43:01 +0200

octave-statistics (1.5.4-2) experimental; urgency=medium

  * test-tolerance.patch: new patch.
    Should fix various test failures on armel, i386 and ppc64el.

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 21 Apr 2023 22:10:57 +0200

octave-statistics (1.5.4-1) experimental; urgency=medium

  [ Rafael Laboissière ]
  * New upstream version 1.5.4
  * d/copyright: Reflect upstream changes
  * d/patches/test-hygecdf-tolerance.patch: Drop patch (applied upstream)

  [ Sébastien Villemot ]
  * Add source lintian override for false positive on docs/svmpredict.html
  * test-normalise_distribution-tolerance.patch: new patch

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 21 Apr 2023 22:10:29 +0200

octave-statistics (1.5.3-2) unstable; urgency=medium

  * d/p/test-hygecdf-tolerance.patch: New patch

 -- Rafael Laboissière <rafael@debian.org>  Wed, 08 Feb 2023 13:30:36 -0300

octave-statistics (1.5.3-1) unstable; urgency=medium

  * New upstream version 1.5.3
  * d/copyright:
    + Reflect upstream changes
    + Drop superfluous stanza
  * d/p/test-tolerance.patch: Drop patch (applied upstream)

 -- Rafael Laboissière <rafael@debian.org>  Mon, 06 Feb 2023 18:54:34 -0300

octave-statistics (1.5.2-2) unstable; urgency=medium

  * New patch: test-tolerance.patch.
    Should fix FTBFS on armhf.

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 25 Dec 2022 12:16:58 +0100

octave-statistics (1.5.2-1) unstable; urgency=medium

  * New upstream version 1.5.2
  * Bump S-V to 4.6.2
  * d/copyright: reflect upstream changes
  * test-tolerance.patch: drop patch, applied upstream
  * d/control: add missing ${shlibs:Depends}

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 19 Dec 2022 19:01:22 +0100

octave-statistics (1.5.1-3) unstable; urgency=medium

  * test-tolerance.patch: yet another tolerance increase, for armel.
    Results vary there across buildds.

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 02 Dec 2022 17:11:56 +0100

octave-statistics (1.5.1-2) unstable; urgency=medium

  * test-tolerance.patch: add more tolerance increases.
    Should fix FTBFS on arm64, i386, ppc64el and s390x.

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 02 Dec 2022 15:02:58 +0100

octave-statistics (1.5.1-1) unstable; urgency=medium

  * New upstream version 1.5.1
  * d/copyright: reflect upstream changes
  * global-install.patch: drop patches, applied upstream
  * test-tolerance.patch: new version of the patch (previous version
    applied upstream)

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 01 Dec 2022 16:36:16 +0100

octave-statistics (1.5.0-2) unstable; urgency=medium

  * test-tolerance.patch: new patch, fixes FTBFS on arm64, i368, ppc64el and
    s390x

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 11 Nov 2022 11:24:47 +0100

octave-statistics (1.5.0-1) unstable; urgency=medium

  [ Rafael Laboissière ]
  * d/watch: Adjust for new URL at gnu-octave.github.io

  [ Sébastien Villemot ]
  * New upstream version 1.5.0
  * d/watch, d/upstream/metadata: repository moved to github.com
  * d/control, d/copyright: homepage moved to Octave Packages
  * d/copyright: reflect upstream changes
  * t_test-skip.patch, fix-cleaning.patch: drop patches, no longer needed
  * d/control: set Architecture to any
  * global-install.patch: new patch taken from upstream
  * Update clean rule
  * test-failures.patch: new patch, disable a couple of failing tests

 -- Sébastien Villemot <sebastien@debian.org>  Tue, 08 Nov 2022 10:59:25 +0100

octave-statistics (1.4.3-3) unstable; urgency=medium

  * d/control: Bump Standards-Version to 4.6.1 (no changes needed)
  * Build-depend on dh-sequence-octave
    + d/control: Ditto
    + d/rules: Drop the --with=octave option from dh call
  * d/p/data-files-for-tests.diff: Drop obsolete patch
  * Proper cleaning of upstream files
    + d/control: Build-depend on dh-octave 1.2.3
    + d/p/fix-cleaning.patch: New patch
    + d/clean: Adjust for new situation
  * d/copyright: Update copyright years of debian/* files
  * d/octave-statistics.lintian-overrides: Adjust for newest version of Lintian

 -- Rafael Laboissière <rafael@debian.org>  Thu, 14 Jul 2022 12:39:02 -0300

octave-statistics (1.4.3-2) unstable; urgency=medium

  * d/p/data-files-for-tests.diff: Convert to DEP-3 format
  * d/rules: Remove workaround for wrong perms in upstream tarball
  * d/clean: Remove empty directories in inst/

 -- Rafael Laboissière <rafael@debian.org>  Sat, 11 Dec 2021 07:41:03 -0300

octave-statistics (1.4.3-1) unstable; urgency=medium

  [ Jenkins ]
  * Remove constraints unnecessary since stretch

  [ Rafael Laboissière ]
  * d/rules: Use execute_before_dh_installdeb instead of override_dh_installdeb
  * d/octave-statistics.lintian-overrides: New file
  * d/copyright: Accentuate my family name
  * d/control: Bump Standards-Version to 4.6.0 (no changes needed)

  [ Sébastien Villemot ]
  * New upstream version 1.4.3
  * d/copyright: reflect upstream changes
  * expose-tbl-delim-tests.patch: drop patch, applied upstream

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 10 Dec 2021 15:48:12 +0100

octave-statistics (1.4.2-2) unstable; urgency=medium

  * d/control: Bump debhelper compatibility level to 13
  * d/u/metadata: Drop Name and Contact fields
  * d/p/expose-tbl-delim-tests.patch: Add URL to Forwarded field
  * d/p/t_test-skip.patch: Set Forwarded: not-needed

 -- Rafael Laboissière <rafael@debian.org>  Wed, 29 Jul 2020 18:30:24 -0300

octave-statistics (1.4.2-1) unstable; urgency=medium

  * New upstream version 1.4.2
  * d/copyright: Reflect upstream changes
  * d/u/metadata: New file
  * d/control: Bump Standards-Version to 4.5.0 (no changes needed)

 -- Rafael Laboissière <rafael@debian.org>  Wed, 01 Apr 2020 08:49:50 -0300

octave-statistics (1.4.1-3) unstable; urgency=medium

  * d/control:
    + Bump Standards-Version to 4.4.1 (no changes needed)
    + Bump dependency on dh-octave to >= 0.7.1
      This allows the injection of the virtual package octave-abi-N
      into the package's list of dependencies.

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 09 Nov 2019 03:40:37 -0300

octave-statistics (1.4.1-2) unstable; urgency=medium

  * Upload to unstable

  [ Rafael Laboissiere ]
  * d/copyright: Fix license name for the AppStream file
  * d/rules: Use override_dh_installdeb instead of _auto_install

  [ Sébastien Villemot ]
  * Bump S-V to 4.4.0

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 08 Jul 2019 15:25:03 +0200

octave-statistics (1.4.1-1) experimental; urgency=medium

  * New upstream version 1.4.1
  * d/p/ttest-tolerance.patch: Drop patch (applied upstream)
  * d/p/t_test-skip.patch: Refresh for new upstream version
  * d/copyright: Reflect upstream changes

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 13 Apr 2019 05:21:17 -0300

octave-statistics (1.4.0-5) unstable; urgency=medium

  * d/control:
    + Bump Standards-Version to 4.3.0 (no changes needed)
    + Bump to debhelper compat level 12
  * Build-depend on debhelper-compat instead of using d/compat

 -- Rafael Laboissiere <rafael@debian.org>  Wed, 02 Jan 2019 22:57:51 -0200

octave-statistics (1.4.0-4) unstable; urgency=medium

  * d/control: Bump Standards-Version to 4.2.1 (no changes needed)
  * d/p/t_test-increase-tol.patch: Drop patch
  * d/p/t_test-skip.patch: New patch

 -- Rafael Laboissiere <rafael@debian.org>  Wed, 12 Sep 2018 04:18:45 -0300

octave-statistics (1.4.0-3) unstable; urgency=medium

  [ Sébastien Villemot ]
  * Revert "d/rules: Use override_dh_installdeb for post-install actions"

  [ Rafael Laboissiere ]
  * d/control:
    + Add Rules-Requires-Root: no
    + Bump Standards-Version to 4.2.0
  * d/p/ttest-tolerance.patch: New patch.
    Allow a tolerance so ttest can pass on i386
    Thanks to Graham Inggs <ginggs@debian.org> (Closes: #906820)
  * d/p/t_test-increase-tol.patch: New patch

 -- Rafael Laboissiere <rafael@debian.org>  Wed, 22 Aug 2018 17:15:38 -0300

octave-statistics (1.4.0-2) unstable; urgency=medium

  * d/control: Build depends on octave >= 4.4

 -- Rafael Laboissiere <rafael@debian.org>  Mon, 11 Jun 2018 15:20:15 -0300

octave-statistics (1.4.0-1) unstable; urgency=medium

  [ Mike Miller ]
  * d/control, d/copyright: Use secure URL for upstream source.

  [ Rafael Laboissiere ]
  * d/rules: Use override_dh_installdeb for post-install actions
  * d/control: Bump Standards-Version to 4.1.4 (no changes needed)

  [ Sébastien Villemot ]
  * New upstream version 1.4.0
  * d/copyright: reflect upstream changes
  * Remove patches applied upstream
    - pval-in-ttest-unit-test.patch
    - tsquare-in-princomp-unit-tests.patch
    - xtests-in-grp2idx.patch
  * Add some generated files to debian/clean

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 20 May 2018 13:01:46 +0200

octave-statistics (1.3.0-4) unstable; urgency=medium

  * Use dh-octave for building the package
  * d/control:
     + Use Debian's GitLab URLs in Vcs-* headers
     + Change Maintainer to team+pkg-octave-team@tracker.debian.org

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 10 Feb 2018 07:38:51 -0200

octave-statistics (1.3.0-3) unstable; urgency=medium

  * Use the dh-based version of octave-pkg-dev
  * Set debhelper compatibility level to >= 11
  * d/control:
    + Bump Standards-Version to 4.1.3 (no changes needed)
    + Add Testsuite field

 -- Rafael Laboissiere <rafael@debian.org>  Fri, 29 Dec 2017 22:14:48 -0200

octave-statistics (1.3.0-2) unstable; urgency=medium

  [ Sébastien Villemot ]
  * d/copyright: use secure URL for format.
  * d/watch: bump to format version 4.

  [ Rafael Laboissiere ]
  * d/control:
    + Use cgit instead of gitweb in Vcs-Browser URL
    + Build-conflict with octave-nan (colision with function 'mad')
    + Bump Standards-Version to 4.1.0 (no changes needed)
    + Build-depends on octave-common > 4.2.1-3. This is necessary
      because this version of the octave-common package
      contains the function corrcoef, necessary for the unit tests.
  * New patches (avoid FTBFS with octave-pkg-dev 1.5.0, closes: #874141):
    + d/p/pval-in-ttest-unit-test.patch
    + d/p/xtests-in-grp2idx.patch
    + d/p/tsquare-in-princomp-unit-tests.patch

 -- Rafael Laboissiere <rafael@debian.org>  Mon, 11 Sep 2017 01:03:46 -0300

octave-statistics (1.3.0-1) unstable; urgency=medium

  [ Sébastien Villemot ]
  * New upstream version 1.3.0
  * Bump debhelper compat level to 10.
  * d/copyright: reflect upstream changes.
  * Drop patches applied upstream.
    + d/p/combnk-implicit-conversion-warn.patch
    + d/p/expected-linkage-warning.patch
    + d/p/small-k-in-gev-functions.patch
    + d/p/use-corr-instead-of-cor.patch
    + d/p/use-corr-instead-of-corrcoef.patch
  * d/rules: add workaround for wrong perms in upstream tarball.

  [ Rafael Laboissiere ]
  * d/control: Use secure URIs in the Vcs-* fields
  * Bump Standards-Version to 3.9.8 (no changes needed)

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 27 Oct 2016 14:32:52 +0200

octave-statistics (1.2.4-1) unstable; urgency=medium

  [ Rafael Laboissiere ]
  * Imported Upstream version 1.2.4
  * d/copyright: Reflect upstream changes
  * Bump Standards-Version to 3.9.6 (no changes needed)
  * Bump build-dependency on octave-pkg-dev, for proper unit testing
  * Bump Build-Depends on octave-io to >> 2.2.4-1
  * d/p/autoload-yes.patch: Remove patch (deprecated upstream)
  * d/p/use-corr-instead-of-cor.patch: New patch
  * d/p/small-k-in-gev-functions.patch: New patch
  * d/p/use-corr-instead-of-corrcoef.patch: New patch
  * d/p/expected-linkage-warning.patch: Add patch
  * d/p/combnk-implicit-conversion-warn.patch: Add patch
  * Add myself to the Uploaders list

  [ Sébastien Villemot ]
  * Remove Thomas Weber from Uploaders.

 -- Rafael Laboissiere <rafael@laboissiere.net>  Sun, 13 Sep 2015 04:00:51 -0300

octave-statistics (1.2.3-1) unstable; urgency=medium

  [ Sébastien Villemot ]
  * Imported Upstream version 1.2.3
  * debian/copyright: reflect upstream changes.
  * princomp-one-arg.patch: new patch, fixes princomp with only one arg.
    (Closes: #731992)

  [ Rafael Laboissiere ]
  * Bump to Standards-Version 3.9.5, no changes needed

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 02 Feb 2014 10:09:28 +0100

octave-statistics (1.2.2-1) unstable; urgency=low

  * Imported Upstream version 1.2.2

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 16 Aug 2013 23:37:27 +0200

octave-statistics (1.2.1-1) unstable; urgency=low

  [ Sébastien Villemot ]
  * Imported Upstream version 1.2.1
  * debian/copyright: reflect upstream changes

  [ Thomas Weber ]
  * debian/control: Use canonical URLs in Vcs-* fields

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 27 Jul 2013 08:28:41 +0200

octave-statistics (1.2.0-2) unstable; urgency=low

  * expose-tbl-delim-tests.patch: new patch, makes sure tbl_delim.m is tested
    (Closes: #672756)

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 18 May 2013 15:09:00 +0200

octave-statistics (1.2.0-1) experimental; urgency=low

  [ Rafael Laboissiere ]
  * Imported Upstream version 1.2.0
  * Bump Standards-Version to 3.9.4 (no changes needed)
  * Use Sébastien Villemot's @debian.org email address
  * Remove obsolete DM-Upload-Allowed flag
  * debian/copyright:
    + Reflect upstream changes
    + Use the octave-maintainers mailing list as upstream contact

  [ Sébastien Villemot ]
  * debian/patches/autoload-yes.patch: new patch

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 16 Jan 2013 10:19:41 +0100

octave-statistics (1.1.3-1) unstable; urgency=low

  * Imported Upstream version 1.1.3

 -- Sébastien Villemot <sebastien.villemot@ens.fr>  Sun, 13 May 2012 14:50:15 +0200

octave-statistics (1.1.2-1) unstable; urgency=low

  * Imported Upstream version 1.1.2
  * debian/control: no longer Build-Depends on octave-miscellaneous
  * add-dependency-on-io.patch: remove patch, applied upstream
  * debian/copyright: reflect upstream changes

 -- Sébastien Villemot <sebastien.villemot@ens.fr>  Wed, 02 May 2012 22:00:26 +0200

octave-statistics (1.1.1-1) unstable; urgency=low

  * Imported Upstream version 1.1.1
  * Remove patches applied upstream:
    + normalise_distribution-index-scalar.patch
    + remove-zscore.patch
    + combnk-cells.patch
    + linkage-clear-lastwarn.patch
  * debian/copyright: reflect upstream changes
  * debian/watch: use SourceForge redirector
  * add-dependency-on-io.patch: new patch, adds dependency on io
  * debian/control: Build-Depends on octave-io >= 1.0.18

 -- Sébastien Villemot <sebastien.villemot@ens.fr>  Tue, 17 Apr 2012 15:14:12 +0200

octave-statistics (1.1.0-1) unstable; urgency=low

  [ Sébastien Villemot ]
  * Imported Upstream version 1.1.0
  * Bump to debhelper compat level 9
  * debian/control:
    + Build-depend on octave-pkg-dev >= 1.0.1, to compile against Octave 3.6
    + Remove Depends and Build-Depends-Indep on octave-gsl
      (expm1 is in Octave core since version 3.2)
    + Add Sébastien Villemot to Uploaders
    + Simplify long description (Octave-Forge was mentionned twice)
    + Remove shlibs:Depends, makes no sense on arch:all package
    + Bump to Standards-Version 3.9.3, no changes needed
  * debian/copyright: upgrade to machine-readable format 1.0
  * debian/source.lintian-overrides: remove file
    (tag build-depends-without-arch-dep no longer exists)
  * debian/patches/remove-zscore.patch: new patch taken from upstream
  * debian/patches/normalise_distribution-index-scalar.patch: new patch
  * debian/patches/combnk-cells.patch: new patch
  * debian/patches/linkage-clear-lastwarn.patch: new patch

 -- Thomas Weber <tweber@debian.org>  Wed, 14 Mar 2012 22:30:15 +0100

octave-statistics (1.0.10-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - Remove Rafael Laboissiere from Uploaders (Closes: #571910)
    - Remove Ólafur Jens Sigurðsson <ojsbug@gmail.com> from Uploaders
  * Bump Standards-Version to 3.8.4 (no changes needed)
  * Switch to dpkg-source 3.0 (quilt) format
  * Dropped patches (applied upstream):
    - fix_bug_with_sprintf
    - remove_outdated_dmult

 -- Thomas Weber <tweber@debian.org>  Sun, 16 May 2010 17:48:37 +0200

octave-statistics (1.0.9-1) unstable; urgency=low

  [ Rafael Laboissiere ]
  * debian/patches/fix-pdist-test.diff: Add patch for making the tests of
    function pdist succeed
  * debian/control: Build-depend on octave-pkg-dev >= 0.7.0, such that the
    package is built against octave3.2

  [ Thomas Weber ]
  * New upstream release
  * Drop patch fix-pdist-test.diff, applied upstream
  * New patches:
    + remove_outdated_dmult: Replace the call to the outdated dmult(). This
      fixes a test failure. Taken from upstream, SVN rev 6725
    + fix_bug_with_sprintf: Correct usage of sprintf, fixing a test failure.
      Taken from upstream, SVN rev 6726.

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Wed, 13 Jan 2010 01:00:18 +0100

octave-statistics (1.0.8-1) unstable; urgency=low

  * New upstream version
  * debian/patches/data-files-for-tests.diff: New patch to add the
    missing *.dat files, needed for the tests of functions caseread and
    tblread.
  * debian/clean: Add file for removing the *write*.dat files created when
    building the package
  * debian/compat: Bump the compatibility level of debhelper to 7,
    otherwise debian/clean does not work
  * debian/rules: Include patchsys-quilt.mk
  * debian/control:
    + (Build-Depends):
      - Add quilt
      - Bump the dependency on debhelper to >= 7
    + (Standards-Version): Bump to 3.8.1 (no changes needed)
    + (Depends): Add ${misc:Depends} and octave-gsl (for expm1)
    + (Vcs-Git, Vcs-Browser): Adjust to new Git repository
    + (Build-Depends-Indep): Add octave-gsl (for expm1, needed in testing
      copulapdf)
  * debian/source.lintian-overrides: Add file for overriding Lintian
    warnings build-depends-without-arch-dep
  * debian/copyright: Use DEP5 URL in Format-Specification
  * debian/README.source: Add file explaining the quilt patch system, as
    required by the Policy

 -- Rafael Laboissiere <rafael@debian.org>  Sun, 24 May 2009 20:23:57 +0200

octave-statistics (1.0.7-1) unstable; urgency=low

  [ Ólafur Jens Sigurðsson ]
  * debian/control: Bumped Standards-Version to 3.8.0 (no changes
    needed)

  [ Rafael Laboissiere ]
  * debian/copyright: Add header
  * debian/control: Bump build-dependency on octave-pkg-dev to >= 0.6.4,
    such that the package is built with the versioned packages directory

  [ Thomas Weber ]
  * New upstream release

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Mon, 06 Apr 2009 23:51:36 +0200

octave-statistics (1.0.6-1) unstable; urgency=low

  [ Ólafur Jens Sigurðsson ]
  * New upstream version

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Thu, 15 May 2008 12:43:43 +0200

octave-statistics (1.0.5-1) unstable; urgency=low

  * Initial release (closes: #468526)

 -- Ólafur Jens Sigurðsson <ojsbug@gmail.com>  Fri, 16 Nov 2007 00:35:04 +0100
